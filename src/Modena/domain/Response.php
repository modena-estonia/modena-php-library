<?php

namespace Modena\Payment\src\Modena\domain;

class Response
{
    /**
     * Response signature verified & transaction was succesful
     */
    const STATUS_SUCCESS = 1;
    /**
     * Response signature verified, but transaction was canceled
     */
    const STATUS_CANCEL = 0;
    /**
     * Response signature could not be verified
     */
    const STATUS_ERROR = -1;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var string
     */
    protected $orderId;

    /**
     * @var int
     */
    protected $transactionId;

    /**
     * @var float
     */
    protected $total;

    /**
     * @param int|null $status
     * @param string|null $stamp
     * @param int|null $transactionId
     * @param float|null $total
     */
    public function __construct($status = null, $stamp = null, $transactionId = null, $total = null)
    {
        $this->status = $status;
        $this->orderId = $stamp;
        $this->transactionId = $transactionId;
        $this->total = $total;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $id
     */
    public function setOrderId($id)
    {
        $this->orderId = $id;
    }

    /**
     * @return string
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $id
     */
    public function setTransactionId($id)
    {
        $this->transactionId = $id;
    }

    /**
     * @return int
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @return bool
     */
    public function isSuccessful()
    {
        return $this->status === self::STATUS_SUCCESS;
    }
}