<?php

namespace Modena\Payment\src\Modena\domain;

use Modena\Payment\src\Modena\config\Config;
use Modena\Payment\src\Modena\config\Fields;
use Modena\Payment\src\Modena\config\Services;
use Modena\Payment\src\Modena\util\Util;

class Request
{
    /**
     * @var string
     */
    protected $sndId;

    /**
     * @var string
     */
    protected $stamp;

    /**
     * @var float
     */
    protected $amount;

    /**
     * @var string
     */
    protected $ref;

    /**
     * @var string
     */
    protected $msg;

    /**
     * @var string
     */
    protected $return;

    /**
     * @var string
     */
    protected $cancel;

    /**
     * @var string
     */
    protected $callback;

    /**
     * @var Order
     */
    protected $order;

    /**
     * @var string
     */
    protected $version;

    /**
     * @var string
     */
    protected $curr;

    /**
     * @var string
     */
    protected $lang;

    /**
     * @var string
     */
    protected $encoding;

    /**
     * @var \DateTime
     */
    protected $dateTime;

    /**
     * @var string
     */
    protected $mac;

    /**
     * @param string $sndId
     * @param string $stamp
     * @param float $amount
     * @param string $ref
     * @param string $msg
     * @param string $return
     * @param string $cancel
     * @param string $callback
     * @param Order $order
     * @param string $version
     * @param string $curr
     * @param string $lang
     * @param string $encoding
     * @throws \Exception
     */
    public function __construct(
        $sndId,
        $stamp,
        $amount,
        $ref,
        $msg,
        $return,
        $cancel,
        $order,
        $callback = '',
        $version = Config::VERSION,
        $curr = Config::DEFAULT_CURRENCY,
        $lang = Config::DEFAULT_LANG,
        $encoding = Config::DEFAULT_ENCODING
    ) {
        $this->sndId = $sndId;
        $this->stamp = $stamp;
        $this->amount = $amount;
        $this->ref = $ref;
        $this->msg = $msg;
        $this->return = $return;
        $this->cancel = $cancel;
        $this->dateTime = new \DateTime('now', new \DateTimeZone('UTC'));
        $this->order = $order;
        $this->callback = $callback;
        $this->version = $version;
        $this->curr = $curr;
        $this->lang = $lang;
        $this->encoding = $encoding;
    }

    public function setMac($mac)
    {
        $this->mac = $mac;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $parameters = [
            Fields::VK_SERVICE => Services::PAYMENT_REQUEST,
            Fields::VK_VERSION => $this->version,
            Fields::VK_SND_ID => $this->sndId,
            Fields::VK_STAMP => $this->stamp,
            Fields::VK_AMOUNT => number_format(round($this->amount, 2), 2, '.', ''),
            Fields::VK_CURR => $this->curr,
            Fields::VK_REF => $this->ref,
            Fields::VK_MSG => $this->msg,
            Fields::VK_RETURN => $this->return,
            Fields::VK_CANCEL => $this->cancel,
            Fields::VK_DATETIME => $this->dateTime->format("Y-m-d\TH:i:sO"),
            Fields::VK_ORDER => json_encode($this->order->toArray()),
            Fields::VK_LANG => $this->lang,
            Fields::VK_ENCODING => $this->encoding,
            Fields::VK_MAC => $this->mac
        ];

        if (Util::isValidURL($this->callback)) {
            $parameters[Fields::VK_CALLBACK] = $this->callback;
        }

        return $parameters;
    }
}