<?php

namespace Modena\Payment\src\Modena\domain;

use Modena\Payment\src\Modena\config\Config;

class Order extends BaseObject
{
    /**
     * @var ClientInfo
     */
    public $client;

    /**
     * @var OrderRow[]
     */
    public $orderRows;

    /**'
     * @var int
     */
    public $paymentLength;

    /**
     * @param ClientInfo $client
     * @param OrderRow[] $orderRows
     * @param int|null $paymentLength
     */
    public function __construct($client, $orderRows, $paymentLength = null)
    {
        $this->client = $client;
        $this->orderRows = $orderRows;
        $this->paymentLength = $paymentLength;

        $this->validate();
    }

    /**
     * @throws \InvalidArgumentException
     */
    private function validate()
    {
        if (!$this->client instanceof ClientInfo) {
            throw new \InvalidArgumentException("Invalid client object");
        }

        foreach ($this->orderRows as $row) {
            if (!$row instanceof OrderRow) {
                throw new \InvalidArgumentException("Invalid orderRows");
            }
        }

        if ($this->paymentLength !== null &&
            ($this->paymentLength < Config::MIN_PAYMENT_LENGTH ||
            $this->paymentLength > Config::MAX_PAYMENT_LENGTH)
        ) {
            throw new \InvalidArgumentException("Invalid paymentLength");
        }
    }
}
