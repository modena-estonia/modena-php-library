<?php

namespace Modena\Payment\src\Modena;

use Modena\Payment\src\Modena\config\Config;
use Modena\Payment\src\Modena\domain\Request;
use Modena\Payment\src\Modena\domain\Response;
use Modena\Payment\src\Modena\service\PaymentRequestService;
use Modena\Payment\src\Modena\service\PaymentResponseService;

class Modena
{

    /**
     * @var string
     */
    protected $merchantPrivateKey;

    /**
     * @var string
     */
    protected $ModenaPublicKey;

    /**
     * @var string
     */
    protected $ModenaAPIURL;

    /**
     * @var string
     */
    protected $merchantKeySecret;

    /**
     * @var string
     */
    protected $merchantRegistryCode;

    /**
     * @var string
     */
    protected $version;

    /**
     * @var PaymentRequestService
     */
    protected $requestService;

    /**
     * @var PaymentResponseService
     */
    protected $responseService;

    /**
     * Modena constructor.
     * @param string $merchantPrivateKey
     * @param string $ModenaPublicKey
     * @param string $ModenaAPIURL
     * @param string $merchantKeySecret
     * @param string $merchantRegistryCode
     * @param string $version
     */
    public function __construct(
        $merchantPrivateKey,
        $ModenaPublicKey,
        $ModenaAPIURL,
        $merchantRegistryCode,
        $merchantKeySecret = '',
        $version = Config::VERSION
    ) {
        $this->merchantPrivateKey = $merchantPrivateKey;
        $this->ModenaPublicKey = $ModenaPublicKey;
        $this->ModenaAPIURL = $ModenaAPIURL;
        $this->merchantKeySecret = $merchantKeySecret;
        $this->merchantRegistryCode = $merchantRegistryCode;
        $this->version = $version;

        $this->requestService = new PaymentRequestService(
            $this->ModenaAPIURL,
            $this->merchantPrivateKey,
            $this->merchantKeySecret
        );
        $this->responseService = new PaymentResponseService($this->ModenaPublicKey, $this->merchantRegistryCode);
    }

    /**
     * @param array $postResponse
     * @return Response
     */
    public function getResponse($postResponse)
    {
        return $this->responseService->processResponse($postResponse);
    }

    /**
     * @param Request $request
     * @param boolean $submit
     * @return string
     */
    public function getRequestForm($request, $submit = true)
    {
        return $this->requestService->generateRequestForm($request, $submit);
    }
}
