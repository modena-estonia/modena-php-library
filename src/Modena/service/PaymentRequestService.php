<?php

namespace Modena\Payment\src\Modena\service;

use Modena\Payment\src\Modena\domain\Request;
use Modena\Payment\src\Modena\util\Util;

class PaymentRequestService
{
    /**
     * @var string
     */
    protected $ModenaAPIURL;

    /**
     * @var string
     */
    protected $merchantPrivateKey;

    /**
     * @var string
     */
    protected $merchantKeySecret;

    /**
     * @param string $ModenaAPIURL
     * @param string $merchantPrivateKey
     * @param string $merchantKeySecret
     */
    public function __construct($ModenaAPIURL, $merchantPrivateKey, $merchantKeySecret = '')
    {
        $this->ModenaAPIURL = $ModenaAPIURL;
        $this->merchantPrivateKey = $merchantPrivateKey;
        $this->merchantKeySecret = $merchantKeySecret;
    }

    /**
     * @param Request $request
     * @return string
     */
    public function generateRequestForm($request, $submit = true)
    {
        $request->setMac($this->getRequestMac($request->toArray()));

        return $this->generateHtml($request, $submit = true);
    }

    /**
     * @param Request $request
     * @param bool $submit
     * @return string
     */
    public function generateHtml($request, $submit = true)
    {
        $html = '<form name="modena" action="' . $this->ModenaAPIURL . '" method="POST">';

        foreach ($request->toArray() as $key => $value) {
            $html .= '<input type="hidden" id="' . strtolower($key) . '" name="' . $key . '" value="' . htmlspecialchars($value) . '"/>';
        }

        $html .= '</form>';

        if ($submit) {
            $html .= '<script>document.modena.submit();</script>';
        }

        return $html;
    }

    /**
     * @param array $data
     * @return string
     */
    private function getRequestMac($data)
    {
        $mac = Util::generateMac($data);

        if ($this->merchantKeySecret) {
            $key = openssl_get_privatekey($this->merchantPrivateKey, $this->merchantKeySecret);
        } else {
            $key = openssl_get_privatekey($this->merchantPrivateKey);
        }

        openssl_sign($mac, $signature, $key);

        openssl_free_key($key);

        return base64_encode($signature);
    }
}