<?php

namespace Modena\Payment\src\Modena\service;

use Modena\Payment\src\Modena\config\Fields;
use Modena\Payment\src\Modena\config\Services;
use Modena\Payment\src\Modena\domain\Response;
use Modena\Payment\src\Modena\util\Util;
use DateTime;
use DateTimeZone;
use Exception;

class PaymentResponseService
{
    /**
     * @var string
     */
    protected $ModenaPublicKey;
    protected $merchantRegistryCode;

    /**
     * @param string $ModenaPublicKey
     * @param $merchantRegistryCode
     */
    public function __construct($ModenaPublicKey, $merchantRegistryCode)
    {
        $this->ModenaPublicKey = $ModenaPublicKey;
        $this->merchantRegistryCode = $merchantRegistryCode;
    }

    /**
     * @param array $responseData
     * @return Response
     */
    public function processResponse($responseData)
    {
        $verificationResult = $this->verifyResponseSignature($responseData);
        return $this->getResponse($responseData, $verificationResult);
    }

    /**
     * @param array $responseData
     * @param bool $verificationResult
     * @return Response
     */
    private function getResponse($responseData, $verificationResult)
    {
        if ($verificationResult) {
            if ($responseData[Fields::VK_REC_ID] !== $this->merchantRegistryCode) {
                error_log('Registry code mismatch, response processing failed...');
                return new Response(Response::STATUS_CANCEL, $responseData[Fields::VK_STAMP], $responseData[Fields::VK_REF]);
            }

            if (!in_array($responseData[Fields::VK_SERVICE], Services::getAcceptedResponses())) {
                error_log('Unsupported service code, response processing failed...');
                return new Response(Response::STATUS_CANCEL, $responseData[Fields::VK_STAMP], $responseData[Fields::VK_REF]);
            }

            if ($responseData[Fields::VK_SERVICE] === Services::PAYMENT_SUCCESS) {
                $status = Response::STATUS_CANCEL;
                try {
                    $signatureDateTime = new DateTime($responseData[Fields::VK_T_DATETIME]);
                    $signatureDateTime = $signatureDateTime->setTimezone(new DateTimeZone('UTC'));
                    $signatureTimestamp = $signatureDateTime->getTimestamp();

                    $now = new DateTime("now", new DateTimeZone('UTC'));
                    $nowTimestamp = $now->getTimestamp();

                    $timestampDifference = $nowTimestamp - $signatureTimestamp;

                    if ($timestampDifference < 0 || $timestampDifference > 1800) {
                        error_log('Signature Timestamp Mismatch...');
                    } else {
                        $status = Response::STATUS_SUCCESS;
                    }
                } catch (Exception $e) {
                    error_log('An exception occurred during the processing of signature timestamp...');
                }
                return new Response($status, $responseData[Fields::VK_STAMP], $responseData[Fields::VK_REF]);
            } else {
                return new Response(Response::STATUS_CANCEL, $responseData[Fields::VK_STAMP], $responseData[Fields::VK_REF]);
            }
        } else {
            return new Response(Response::STATUS_ERROR);
        }
    }

    /**
     * @param array $responseData
     * @return bool
     */
    private function verifyResponseSignature($responseData)
    {
        $mac = Util::generateMac($responseData);

        $key = openssl_get_publickey($this->ModenaPublicKey);
        $result = openssl_verify($mac, base64_decode($responseData[Fields::VK_MAC]), $key);
        openssl_free_key($key);

        return $result === 1;
    }
}