<?php

namespace Modena\Payment\src\Modena\config;

final class Fields
{
    const VK_SERVICE = 'VK_SERVICE';
    const VK_VERSION = 'VK_VERSION';
    const VK_SND_ID = 'VK_SND_ID';
    const VK_REC_ID = 'VK_REC_ID';
    const VK_STAMP = 'VK_STAMP';
    const VK_T_NO = 'VK_T_NO';
    const VK_AMOUNT = 'VK_AMOUNT';
    const VK_CURR = 'VK_CURR';
    const VK_REC_ACC = 'VK_REC_ACC';
    const VK_REC_NAME = 'VK_REC_NAME';
    const VK_SND_ACC = 'VK_SND_ACC';
    const VK_SND_NAME = 'VK_SND_NAME';
    const VK_REF = 'VK_REF';
    const VK_MSG = 'VK_MSG';
    const VK_RETURN = 'VK_RETURN';
    const VK_CANCEL = 'VK_CANCEL';
    const VK_DATETIME = 'VK_DATETIME';
    const VK_T_DATETIME = 'VK_T_DATETIME';
    const VK_ORDER = 'VK_ORDER';
    const VK_CALLBACK = 'VK_CALLBACK';
    const VK_MAC = 'VK_MAC';
    const VK_ENCODING = 'VK_ENCODING';
    const VK_LANG = 'VK_LANG';
    const VK_AUTO = 'VK_AUTO';
}
