<?php

namespace Modena\Payment\src\Modena\config;

final class Services
{
    const PAYMENT_REQUEST = '1012';
    const PAYMENT_SUCCESS = '1111';
    const PAYMENT_CANCEL = '1911';

    /**
     * @param string $serviceId
     * @return string[]
     */
    public static function getFieldsForService($serviceId)
    {
        switch ($serviceId) {
            case Services::PAYMENT_REQUEST:
                return [
                    Fields::VK_SERVICE,
                    Fields::VK_VERSION,
                    Fields::VK_SND_ID,
                    Fields::VK_STAMP,
                    Fields::VK_AMOUNT,
                    Fields::VK_CURR,
                    Fields::VK_REF,
                    Fields::VK_MSG,
                    Fields::VK_RETURN,
                    Fields::VK_CANCEL,
                    Fields::VK_DATETIME,
                    Fields::VK_ORDER,
                    Fields::VK_CALLBACK
                ];
            case Services::PAYMENT_SUCCESS:
                return [
                    Fields::VK_SERVICE,
                    Fields::VK_VERSION,
                    Fields::VK_SND_ID,
                    Fields::VK_REC_ID,
                    Fields::VK_STAMP,
                    Fields::VK_T_NO,
                    Fields::VK_AMOUNT,
                    Fields::VK_CURR,
                    Fields::VK_REC_ACC,
                    Fields::VK_REC_NAME,
                    Fields::VK_SND_ACC,
                    Fields::VK_SND_NAME,
                    Fields::VK_REF,
                    Fields::VK_MSG,
                    Fields::VK_T_DATETIME
                ];
            case Services::PAYMENT_CANCEL:
                return [
                    Fields::VK_SERVICE,
                    Fields::VK_VERSION,
                    Fields::VK_SND_ID,
                    Fields::VK_REC_ID,
                    Fields::VK_STAMP,
                    Fields::VK_REF,
                    Fields::VK_MSG
                ];
            default:
                throw new \InvalidArgumentException("Unsupported service id: {$serviceId}");
        }
    }

    /**
     * @return string[]
     */
    public static function getAcceptedResponses()
    {
        return [
            self::PAYMENT_SUCCESS,
            self::PAYMENT_CANCEL
        ];
    }
}
