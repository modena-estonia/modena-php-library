<?php

namespace Modena\Payment\src\Modena\config;

class Config
{
    const VERSION = '008';

    const MIN_PAYMENT_LENGTH = 1;

    const MAX_PAYMENT_LENGTH = 3;

    const DEFAULT_CURRENCY = 'EUR';

    const DEFAULT_LANG = 'EST';

    const DEFAULT_ENCODING = 'UTF-8';
}
